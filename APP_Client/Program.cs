﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_Client
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomModule mqtt = null;
            //if (args.Length < 1)
            //{
            //    Console.WriteLine("Usage: {0} <path-to-script>", AppDomain.CurrentDomain.FriendlyName);
            //    Console.WriteLine("Error: you need to add on the script launched");
            //    Environment.Exit(0); //exitcode: 0 is success, anything else indicates an error, ref: http://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx
            //}

            mqtt = new CustomModule
            {
                brokerAddress = ConfigurationManager.AppSettings["SERVER"],
                brokerPort = Convert.ToInt32(ConfigurationManager.AppSettings["PORT"]),
                username = ConfigurationManager.AppSettings["USER"],
                passsword = ConfigurationManager.AppSettings["PASS"],
                //csvPath = args[0]
            };

            try
            {
                if(ConfigurationManager.AppSettings["AUTOSEARCH"] == "true")
                {
                    /// launch DNS discovery service
                    var dns_service = new DiscoverDNS.DiscoverDNS(); //AUTOSEARCH
                    dns_service.BasedQuery("mqtt");
                    while (true)
                        if (dns_service.ipAddress != string.Empty)
                            break;
                    mqtt.brokerAddress = dns_service.ipAddress;
                }
                if (ConfigurationManager.AppSettings["SERVER"] != "")
                    mqtt.brokerAddress = ConfigurationManager.AppSettings["SERVER"];

                //mqtt.Init();
                /// set Topic
                //Dictionary<string, int> topicList = new Dictionary<string, int>();
                //topicList.Add(ConfigurationManager.AppSettings["TOPIC"], Int32.Parse(ConfigurationManager.AppSettings["SUBSCRIBE_QoS"].ToString()));
                //mqtt.setSubscribe_List(topicList);
                mqtt.setSubscribe(ConfigurationManager.AppSettings["TOPIC"], Int32.Parse(ConfigurationManager.AppSettings["SUBSCRIBE_QoS"].ToString()));
                mqtt.ClientInit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Error] " + ex.Message);
                Environment.Exit(0);
            }

            while (mqtt.IsConntected())
            {
                var inputString = Console.ReadLine().Trim();

                if (inputString.ToLower() == "exit")
                {
                    Console.WriteLine("bye bye！");
                    break;
                }

            }
            System.Threading.Thread.Sleep(1000);
#if false
            string inputString, topic_send, topic_receive = string.Empty;

            Console.WriteLine("enter \"help\" if you don't know how to do.");
            while (true)
            {
                inputString = Console.ReadLine().ToLower().Trim();

                if (inputString == "help")
                {
                    Console.WriteLine("exit : exit the program");
                    Console.WriteLine("clear : clear messages from the window");
                    Console.WriteLine("publish : send the message with the topic you set");
                    Console.WriteLine("subscribe : receive the message with the topic you set");
                    Console.WriteLine("unsubcribe : unsubcribe the message with the topic you set");
                }
                switch (inputString)
                {
                    case "exit":
                        Console.WriteLine("MQTT Client exit！");
                        Environment.Exit(1);
                        break;
                    case "clear":
                        Console.Clear();
                        Console.WriteLine("enter \"help\" if you don't know how to do.");
                        break;
                    case "publish":
                        Console.WriteLine("Input Topic Sended: ");
                        topic_send = Console.ReadLine().ToLower().Trim();
                        while (true)
                        {
                            Console.WriteLine("Input Message: ");
                            var msg = Console.ReadLine().ToLower().Trim();
                            if (msg == "exit")
                                break;
                            mqtt.Publish(topic_send, msg);
                        }
                        break;
                    case "subscribe":
                        Console.WriteLine("Input Topic Received: ");
                        topic_receive = Console.ReadLine().ToLower().Trim();
                        mqtt.Subscribe(topic_receive);
                        //while (true)
                        //{
                        //    inputString = Console.ReadLine().ToLower().Trim();
                        //    if (inputString == "exit")
                        //        break;
                        //}
                        break;
                    case "unsubcribe":
                        Console.WriteLine("Input Topic Received: ");
                        topic_receive = Console.ReadLine().ToLower().Trim();
                        mqtt.Unsubscribe(topic_receive);

                        break;
                }
                //Console.WriteLine("Press any key to exit.");
                //Console.ReadLine();
            }
#endif
        }
    }

}
