﻿//#define RBRIDGE

using APP_Client.Module;
using Geosat.Mqtt;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_Client
{
    enum Action
    {
        ROBOTBODYMOVE = 1,
        ROBOTNECKMOVE,
        EMOTION,
        TPRINTER,
        PLAYVIDEO,
        API,
        CMD,
        SHOWTEXT

    }
    public class CustomModule : MqttModule
    {
        //CustomModule mqtt = new CustomModule();
        string serverTopic = "server";
        DBModule db;
        CmdModule cli = new CmdModule();
        public string csvPath { get; set; } = string.Empty;
        /// <summary>
        /// Robot webAPI
        /// </summary>
        //string url = "http://127.0.0.1:8080/GeosatRobot/api/Device";
        string url = ConfigurationManager.AppSettings["API_URL"].ToString();
        //string deviceID = "test";
        //string action = "start";
        //string cmd = string.Empty;
        int PUBLICH_QoS = Int32.Parse(ConfigurationManager.AppSettings["PUBLICH_QoS"].ToString());
        string log = string.Empty;
        Task<string> t_response = null;
        public override async Task MqttClientReceived()
        {
            mqttClient.ApplicationMessageReceived += async (s, e) =>
            {
                //lock (locked)
                //{
                var msg = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
                Console.WriteLine($"From Topic [{e.ApplicationMessage.Topic}]");
                Console.WriteLine(msg);
                string[] contents = msg.Split('>');
                //string[] missionCount = null;
                string work = string.Empty;
                if (contents[0].ToLower().Trim() == ConfigurationManager.AppSettings["CLIENT_ID"].ToLower())
                {
                    try
                    {
                        PrintToConsole($"[ {contents[0].ToUpper()} ] {contents[1]}\n", ConsoleColor.Green);

                        switch (Enum.Parse(typeof(Action), contents[1].ToString()))
                        {
                            case Action.ROBOTBODYMOVE:
                                PrintToConsole(work = $"Action.ROBOTBODYMOVE {contents[2]}\n", ConsoleColor.Yellow);
                                t_response = Task.Run(async () =>
                                {
                                    work = POST(url, "ROBOTBODYMOVE", "start", contents[2]).Content + "\n";
                                    PrintToConsole(work, ConsoleColor.Cyan);
                                    return work;
                                });
                                work = t_response.Result.ToString();
                                break;
                            case Action.ROBOTNECKMOVE: // old 
                                PrintToConsole(work = $"Action.ROBOTNECKMOVE {contents[2]}\n", ConsoleColor.Yellow);
                                t_response = Task.Run(async () =>
                                {
                                    work = POST(url, "ROBOTNECKMOVE", "start", contents[2]).Content + "\n";
                                    PrintToConsole(work, ConsoleColor.Cyan);
                                    await Task.Delay(TimeSpan.FromMilliseconds(float.Parse(contents[3]) * 1000));
                                    POST(url, "ROBOTNECKMOVE", "start", "0/0");
                                    return work;
                                });
                                work = t_response.Result.ToString();
                                break;
                            case Action.EMOTION:
                                PrintToConsole(work = $"Action.EMOTION {contents[2]}\n", ConsoleColor.Yellow);
                                t_response = Task.Run(async () =>
                                {
                                    //POST(url, "EMOTION", "start", missionCount[2]);
                                    PrintToConsole(work = POST(url, "EMOTION", "start", contents[2]).Content + "\n", ConsoleColor.Cyan);
                                    return work;
                                });
                                work = t_response.Result.ToString();
                                break;
                            ////case Action.TPRINTER:
                            ////    PrintToConsole(work = $"Action.TPRINTER {missionCount[2]}\n", ConsoleColor.Yellow);
                            ////    t_response = Task.Run(async () =>
                            ////    {
                            ////            //POST(url, "EMOTION", "start", missionCount[2]);
                            ////            PrintToConsole(work = POST(url, "TPRINTER", "start", missionCount[2]).Content + "\n", ConsoleColor.Cyan);
                            ////        return work;
                            ////    });
                            ////    work = t_response.Result.ToString();
                            ////    break;
                            case Action.PLAYVIDEO:
                                PrintToConsole(work = $"Action.PLAYVIDEO {contents[2]}\n", ConsoleColor.Yellow);
                                Task.Run(async () =>
                                {
                                    Publish("video", contents[2], PUBLICH_QoS);
                                });
                                break;
                            case Action.API:
                                PrintToConsole(work = $"Action.API {contents[2]}\n", ConsoleColor.Yellow);
                                t_response = Task.Run(async () =>
                                {
                                    ///對應API指令狀態：deviceID/start(or stop)/content
                                    string[] post_item = contents[2].Split('/');
                                    work = post_item.Length > 2
                                        ? POST(url, post_item[0], post_item[1], post_item[2]).Content + "\n"
                                        : POST(url, post_item[0], post_item[1]).Content.ToString() + "\n";

                                    PrintToConsole(work, ConsoleColor.Cyan);
                                    return work;
                                });
                                work = t_response.Result.ToString();
                                break;
                            case Action.CMD:
                                PrintToConsole(work = $"Action.CMD {contents[2]}\n", ConsoleColor.Yellow);
                                Task.Run(() =>
                                {
                                    // only launch the program through the cmd
                                    cli.CommandOutput(contents[2]); 
                                });
                                break;
                            case Action.SHOWTEXT:
                                PrintToConsole(work = $"Action.SHOWTEXT {contents[2]}\n", ConsoleColor.Yellow);
                                //Task.Run(async () =>
                                //{
                                //    await Task.Delay(1000);
                                //    PrintToConsole("Task.Delay");
                                //});
                                break;
                        }
                        //PrintToConsole("1  " + work, ConsoleColor.Cyan);

                        Publish(serverTopic, $"Server,200,{contents[0].ToUpper()},{contents[1]} {contents[2]} accomplished", PUBLICH_QoS);

                        log = @"Client id:" + e.ClientId + "\n"
                                + "Time：" + DateTime.Now + "\n"
                                + "RobotID：" + ConfigurationManager.AppSettings["CLIENT_ID"].ToString() + "\n"
                                + "Msg：" + work + "\n"
                                + "--------------------------------------------------\n";
                        WriteMsgLog(log);

                    }
                    catch (Exception ex)
                    {
                        PrintToConsole(ErrorMsg(ex.Message) + "\n", ConsoleColor.Red);

                        log = @"Client id:" + e.ClientId + "\n"
                                    + "Time：" + DateTime.Now + "\n"
                                    + "RobotID：" + ConfigurationManager.AppSettings["CLIENT_ID"].ToString() + "\n"
                                    + "Msg：" + ErrorMsg(ex.Message) + "\n"
                                    + "--------------------------------------------------\n";
                        WriteMsgLog(log);
                        //work = null;
                    }
                }
                //}
            };
        }

#if !RBRIDGE
        public IRestResponse POST(string url, string deviceId, string action, string cmd = null)
#else
        static IRestResponse POST(string url, string parameter)
#endif
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Token", Guid.NewGuid().ToString());
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
#if !RBRIDGE
            request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request.AddParameter($"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"paraString\"\r\n\r\n{ \n   \"time\":\"" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\"\n , \"requestId\":\"geosat\",\n   \"action\":\"" + action + "\",\n\"deviceId\":\"" + deviceId + "\",\n   \"content\":\"" + cmd + "\"}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
#else
            request.AddParameter($"undefined", parameter + "&undefined=", ParameterType.RequestBody);
#endif
            IRestResponse response = client.Execute(request);
            return response;
        }

        /// <summary>
        /// 將CSV文件的數據讀取到DataTable中
        /// </summary>
        /// <param name="fileName">CSV文件路徑</param>
        /// <returns>返回讀取了CSV數據的DataTable</returns>
        public DataTable LoadCSV(string filePath)
        {
            DataTable dt = new DataTable();
            FileStream fs = new FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            StreamReader sr = new StreamReader(fs, Encoding.Default);
            //StreamReader sr = new StreamReader(fs, encoding);
            //string fileContent = sr.ReadToEnd();
            //記錄每次讀取的一行記錄
            string strLine = "";
            //記錄每行記錄中的各字段內容
            string[] aryLine = null;
            string[] tableHead = null;
            //標示列數
            int columnCount = 0;
            //標示是否是讀取的第一行
            bool IsFirst = true;
            //逐行讀取CSV中的數據
            while ((strLine = sr.ReadLine()) != null)
            {
                if (IsFirst == true)
                {
                    tableHead = strLine.Split(',');
                    IsFirst = false;
                    columnCount = tableHead.Length;
                    //創建列
                    for (int i = 0; i < columnCount; i++)
                    {
                        tableHead[i] = tableHead[i].Replace("\"", "");
                        DataColumn dc = new DataColumn(tableHead[i]);
                        dt.Columns.Add(dc);
                    }
                }
                else
                {
                    aryLine = strLine.Split(',');
                    DataRow dr = dt.NewRow();
                    for (int j = 0; j < columnCount; j++)
                    {
                        dr[j] = aryLine[j].Replace("\"", "");
                    }
                    dt.Rows.Add(dr);
                }
            }
            //if (aryLine != null && aryLine.Length > 0)
            //{
            //    dt.DefaultView.Sort = tableHead[2] + " " + "DESC";
            //}
            sr.Close();
            fs.Close();
            return dt;
        }
        /// <summary>
        /// Database initailize
        /// </summary>
        public void Init()
        {
            var dt = LoadCSV(csvPath);
            db = new DBModule
            {
                path = @"Show.db",
                tableScript = @"Script",
                dt = dt
            };
            db.InitDB();

            var result = db.ImportCSVtoDB(csvPath);
            Console.WriteLine(result);
            PrintToConsole(POST(url, "test", "start").Content + "\n", ConsoleColor.Cyan);

            //Console.WriteLine(db.ReadTable("3"));

            //List<Tuple<string, string, string, string>> listDT = new List<Tuple<string, string, string, string>>();
            //foreach (System.Data.DataRow row in dt.Rows)
            //{
            //    string Step = row["Step"].ToString();
            //    string RobotID = row["Robot NO"].ToString();
            //    string Action = row["Action"].ToString();
            //    string Description = row["Description"].ToString();
            //    string ActionTime = row["Action time(Sec)"].ToString();
            //    string Speed = row["Speed(-1.2~1.2)"].ToString();
            //    Console.WriteLine(Step + " | " + RobotID + " | " + Action + " | " + Description + " | " + ActionTime + " | " + Speed);
            //    listDT.Add(new Tuple<string, string, string, string>(Step, Action, ActionTime, Speed));
            //}
            //foreach (var item in listDT)
            //{
            //    Console.WriteLine(item.Item1 + " | " + item.Item2 + " | " + item.Item3 + " | " + item.Item4);
            //}
        }

        public override void WriteMsgLog(string msg)
        {
            string path = currentPath + "\\Clientlog.txt";
            FileInfo file = new FileInfo(path);
            if (!file.Exists)
            {
                FileStream fs;
                fs = File.Create(path);
                fs.Close();
            }
            using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(DateTime.Now.ToString() + "   " + msg);
                }
            }
        }
    }
}
