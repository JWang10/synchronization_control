﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_Client.Module
{
    public class CMD
    {
        /// <summary>
        /// Use command line to do windows' scripts (and with administrator)
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="UseAdmin"></param>
        /// <returns></returns>
        public string CommandOutput(string commandText, bool UseAdmin = false)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            if (UseAdmin)
                p.StartInfo.Verb = "runas";
            p.StartInfo.CreateNoWindow = true; //don't show cmd window
            string strOutput = null;

            try
            {
                p.Start();
                p.StandardInput.WriteLine(commandText);
                p.StandardInput.WriteLine("exit");
                p.StandardInput.AutoFlush = true;
                strOutput = p.StandardOutput.ReadToEnd();//pass all of process
                p.WaitForExit();
                p.Close();
                //sw.Close();

            }
            catch (Exception e)
            {
                strOutput = Common.FailureMessage(e.Message);
            }
            return strOutput;
        }
    }
}
