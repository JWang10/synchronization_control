﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace APP_Client
{
    public class DBModule
    {

        public string path { get; set; } = string.Empty;
        public string tableScript { get; set; } = string.Empty;
        public DataTable dt { get; set; } = new DataTable();
        private SQLiteConnection sqlite_conn;
        private SQLiteCommand sqlite_cmd;
        static StringBuilder sb;

        public enum ShowDBCommand
        {
            ReadScriptList = 1,
            CheckScriptCount,
            ClearicashTable,
            ImportCSV,
            ExportCSV
        }

        public void InitDB()
        {
            sqlite_conn = new SQLiteConnection("data source=" + path);
            sb = new StringBuilder();
        }

        public string ClearicashTable()
        {
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();
            using (var transaction = sqlite_conn.BeginTransaction())
            {
                sqlite_cmd.CommandText = $@"DELETE FROM icashCard";
                sqlite_cmd.ExecuteNonQuery();
                transaction.Commit();
            }
            sqlite_conn.Close();
            return "Clear icashTable successfully";
        }
        public string ReadTable(string step)
        {
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT * FROM {tableScript} WHERE Step={step}";

            SQLiteDataReader reader = sqlite_cmd.ExecuteReader();
            sb.Clear();
            //print all field content
            while (reader.Read())
            {
                for (int i = 1; i < reader.FieldCount; i++)
                {
                    //Console.Write($"{reader[i]},"
                    sb.Append(reader[i].ToString().Trim());
                    if (i < reader.FieldCount - 1)
                        sb.Append(",");
                    //sb.Append(reader["Action"].ToString().Trim());
                    //sb.Append(reader["Description"].ToString().Trim());
                    //sb.Append(reader["Action time(Sec)"].ToString().Trim());
                    //sb.Append(reader["Speed(-1.2~1.2)"].ToString().Trim());

                }
                sb.Append("/");
                //Console.WriteLine();
            }
            reader.Close();
            sqlite_conn.Close();
            return sb.ToString();
        }

        // HARD CORE .... really badddd
        public string ImportCSVtoDB(string csvFile)
        {
            string[] csvColumnNames_Script ={
                    "Step", "Robot NO", "Action","Description","Action time(Sec)", "Speed(-1.2~1.2)"
                    //"Step", "RobotID", "Action","Description","ActionTime", "Speed"
               };

            try
            {
                if (!File.Exists(csvFile))
                    return "failure, check if your file path is correct";
                try
                {
                    SaveCsvtoDB(csvFile, path, tableScript, csvColumnNames_Script);
                }
                catch (Exception ex)
                {
                    return ErrorMsg(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return "failure, " + ex.Message;
            }
            return $"Import {Path.GetFileName(csvFile)} successfully";
        }
        private void SaveCsvtoDB(string csvFile, string path, string databaseTableName, string[] csvColumnNames)
        {

            SQLiteConnection sqlite_conn = new SQLiteConnection($"Data source={path}");
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();

            using (var transaction = sqlite_conn.BeginTransaction())
            {
                try
                {

                    sqlite_cmd.CommandText = $@"DELETE FROM {databaseTableName}";
                    sqlite_cmd.ExecuteNonQuery();


                    foreach (DataRow dr in dt.Rows)
                    {
                        Dictionary<string, string> columnItem = new Dictionary<string, string>();
                        //no test
                        //List < KeyValuePair<string, SQLiteParameter[]> > list = new List<KeyValuePair<string, SQLiteParameter[]>>();
                        foreach (DataColumn dc in dt.Columns)
                        {
                            columnItem.Add(dc.ColumnName, dr[dc.ColumnName].ToString());
                        }
                        //foreach (var item in columnItem)
                        //    Console.Write(item.Key + " , " + item.Value + " , ");

                        /*HARD CORE ...really badddd*/
                        if (csvColumnNames.Length <= 5)
                        {
                            sqlite_cmd.CommandText = $"INSERT INTO {databaseTableName} ('{columnItem.ElementAt(0).Key}','{columnItem.ElementAt(1).Key}'," +
                                    $"'{columnItem.ElementAt(2).Key}','{columnItem.ElementAt(3).Key}','{columnItem.ElementAt(4).Key}') VALUES ('{columnItem.ElementAt(0).Value}','{columnItem.ElementAt(1).Value}'," +
                                    $"'{columnItem.ElementAt(2).Value}','{columnItem.ElementAt(3).Value}','{columnItem.ElementAt(4).Value}')";
                        }
                        else
                        {
                            sqlite_cmd.CommandText = $"INSERT INTO {databaseTableName} ('{columnItem.ElementAt(0).Key}','{columnItem.ElementAt(1).Key}'," +
                                    $"'{columnItem.ElementAt(2).Key}','{columnItem.ElementAt(3).Key}','{columnItem.ElementAt(4).Key}','{columnItem.ElementAt(5).Key}') VALUES ('{columnItem.ElementAt(0).Value}','{columnItem.ElementAt(1).Value}'," +
                                    $"'{columnItem.ElementAt(2).Value}','{columnItem.ElementAt(3).Value}','{columnItem.ElementAt(4).Value}','{columnItem.ElementAt(5).Value}')";

                        }
                        sqlite_cmd.ExecuteNonQuery();

                    }

                    //            foreach (System.Data.DataRow row in dt.Rows)
                    //            {
                    //                string Step = row[csvColumnNames[0]].ToString();
                    //                string RobotID = row[csvColumnNames[1]].ToString();
                    //                string Action = row[csvColumnNames[2]].ToString();
                    //                string Description = row[csvColumnNames[3]].ToString();
                    //                string ActionTime = row[csvColumnNames[4]].ToString();
                    //                string Speed = row[csvColumnNames[5]].ToString();
                    //                Console.WriteLine(Step + " | " + RobotID + " | " + Action + " | " + Description + " | " + ActionTime + " | " + Speed);

                    //                sqlite_cmd.CommandText = $"INSERT INTO {databaseTableName} ('{csvColumnNames[0]}','{csvColumnNames[1]}'," +
                    //$"'{csvColumnNames[2]}','{csvColumnNames[3]}','{csvColumnNames[4]}','{csvColumnNames[5]}') VALUES ('{Step}','{RobotID}'," +
                    //$"'{Action}','{Description}','{ActionTime}','{Speed}')";

                    //                sqlite_cmd.ExecuteNonQuery();

                    //            }
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            sqlite_conn.Close();
        }

        private string ErrorMsg(string msg)
        {
            return "[ ERROR ] :" + msg;
        }
    }
}
