﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace APP_Server
{
    public class DBModule
    {

        public string path { get; set; } = string.Empty;
        public string tableScript { get; set; } = string.Empty;
        public DataTable dt { get; set; } = new DataTable();
        private SQLiteConnection sqlite_conn;
        private SQLiteCommand sqlite_cmd;
        static StringBuilder sb;

        public enum ShowDBCommand
        {
            ReadScriptList = 1,
            CheckScriptCount,
            ClearicashTable,
            ImportCSV,
            ExportCSV
        }

        public void InitDB()
        {
            sqlite_conn = new SQLiteConnection("data source=" + path);
            sb = new StringBuilder();
        }

        public string ClearicashTable()
        {
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();
            using (var transaction = sqlite_conn.BeginTransaction())
            {
                sqlite_cmd.CommandText = $@"DELETE FROM icashCard";
                sqlite_cmd.ExecuteNonQuery();
                transaction.Commit();
            }
            sqlite_conn.Close();
            return "Clear icashTable successfully";
        }
        public string ReadTable(string whichColumnName, string step, bool last = false)
        {
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();
            if (!last)
                sqlite_cmd.CommandText = $"SELECT * FROM {tableScript} WHERE Step={step}";
            else
                sqlite_cmd.CommandText = $"SELECT * FROM {tableScript} ORDER BY Step DESC limit 1";

            SQLiteDataReader reader = sqlite_cmd.ExecuteReader();
            sb.Clear();
            //print all field content
            while (reader.Read())
            {

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    //Console.Write($"{Convert.ToString(reader[i])}");
                    sb.Append(reader[i].ToString().Trim());
                    if (i < reader.FieldCount - 1)
                        sb.Append(",");
                    //sb.Append(reader["Action"].ToString().Trim());
                    //sb.Append(reader["Description"].ToString().Trim());
                    //sb.Append(reader["Action time(Sec)"].ToString().Trim());
                    //sb.Append(reader["Speed(-1.2~1.2)"].ToString().Trim());

                }
                sb.Append("/");
                //Console.WriteLine();
            }
            reader.Close();
            sqlite_conn.Close();
            return sb.ToString();
        }
        public string ReadTableCount(string whichColumnName)
        {
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT COUNT({whichColumnName}) FROM {tableScript}";
            int result = 0;
            var RowCount = sqlite_cmd.ExecuteScalar();
            if (RowCount != null)
            {
                result = Convert.ToInt32(sqlite_cmd.ExecuteScalar());
            }
            sqlite_conn.Close();
            return result.ToString();

        }

        public string UpdateServerStatus(string robotID)
        {
            var table = "ServerStatus";
            var status = false;
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();
            using (var transaction = sqlite_conn.BeginTransaction())
            {
                try
                {
                    status = true;
                    //sqlite_cmd.CommandText = $"UPDATE {table} SET \"RobotID\" = [\"{robotID}\"]\",\"Status\" = [{status}] WHERE Step={step}";
                    sqlite_cmd.CommandText = $"UPDATE {table} SET \"Status\" = [{status}] WHERE RobotID={robotID}";
                    sqlite_cmd.ExecuteNonQuery();

                }
                catch (SQLiteException ex)
                {
                    transaction.Rollback();
                    status = false;
                    return ErrorMsg(ex.Message);
                }
                finally
                {
                    status = false;
                }
            }
            sqlite_conn.Close();

            return "Update status successfully";
        }
        public string ImportRobotIDtoDB(string[] columnHead)
        {
            var table = "ServerStatus";
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();
            using (var transaction = sqlite_conn.BeginTransaction())
            {
                try
                {
                    sqlite_cmd.CommandText = $@"DELETE FROM {table};";
                    sqlite_cmd.ExecuteNonQuery();

                    var status = false;
                    for (var i = 1; i < columnHead.Length; i++)
                    {
                        sqlite_cmd.CommandText = $"INSERT INTO {table}( RobotID, Status) VALUES ( '{columnHead[i]}', '{status}')";
                        sqlite_cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch (SQLiteException ex)
                {
                    transaction.Rollback();
                    return ErrorMsg(ex.Message);
                }

            }
            sqlite_conn.Close();
            return "Import status successfully";
        }

        // HARD CORE .... really badddd
        public string ImportCSVtoDB(string csvFile, string[] columnHead)
        {
            //string[] csvColumnNames_Script ={
            //        "Step", "C1","C2"
            //   };

            try
            {
                if (!File.Exists(csvFile))
                    return "failure, check if your file path is correct";
                try
                {
                    SaveCsvtoDB(csvFile, path, tableScript, columnHead);
                }
                catch (Exception ex)
                {
                    return ErrorMsg(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return "failure, " + ex.Message;
            }
            return $"Import {Path.GetFileName(csvFile)} successfully";
        }
        private void SaveCsvtoDB(string csvFile, string path, string databaseTableName, string[] columnHead)
        {

            SQLiteConnection sqlite_conn = new SQLiteConnection($"Data source={path}");
            sqlite_conn.Open();
            sqlite_cmd = sqlite_conn.CreateCommand();

            using (var transaction = sqlite_conn.BeginTransaction())
            {
                try
                {
                    string columnContent = null;
                    for (int i = 0; i < columnHead.Length; i++)
                    {
                        columnContent += columnHead[i] + " TEXT";
                        if (i < columnHead.Length - 1)
                        {
                            columnContent += ", ";
                        }
                    }
                    sqlite_cmd.CommandText = $@"PRAGMA foreign_keys = 0;"
                                             //+ $"CREATE TABLE temp_table AS SELECT * FROM {databaseTableName};"
                                             + $"DROP TABLE {databaseTableName};"
                                             + $@"CREATE TABLE {databaseTableName}({columnContent});"
                                             //+ $@"INSERT INTO {databaseTableName} (Step,C1) SELECT Step,C1 FROM temp_table;"
                                             + $@"PRAGMA foreign_keys = 1;";

                    sqlite_cmd.ExecuteNonQuery();
                    columnContent = null;

                    string columnItemKeys = null;
                    string columnItemValues = null;

                    foreach (DataRow dr in dt.Rows)
                    {
                        Dictionary<string, string> columnItem = new Dictionary<string, string>();

                        foreach (DataColumn dc in dt.Columns)
                        {
                            columnItem.Add(dc.ColumnName, dr[dc.ColumnName].ToString());
                            columnItemKeys += "'" + dc.ColumnName + "'";
                            columnItemValues += "'" + dr[dc.ColumnName].ToString() + "'";
                            if (dt.Columns.IndexOf(dc)< dt.Columns.Count - 1)
                            {
                                columnItemKeys += ", ";
                                columnItemValues += ", ";
                            }
                        }
                        sqlite_cmd.CommandText = $"INSERT INTO {databaseTableName} ({columnItemKeys}) VALUES ({columnItemValues})";

                        sqlite_cmd.ExecuteNonQuery();
                        columnItemKeys = null;
                        columnItemValues = null;
                    }

                    //            foreach (System.Data.DataRow row in dt.Rows)
                    //            {
                    //                string Step = row[csvColumnNames[0]].ToString();
                    //                string RobotID = row[csvColumnNames[1]].ToString();
                    //                string Action = row[csvColumnNames[2]].ToString();
                    //                string Description = row[csvColumnNames[3]].ToString();
                    //                string ActionTime = row[csvColumnNames[4]].ToString();
                    //                string Speed = row[csvColumnNames[5]].ToString();
                    //                Console.WriteLine(Step + " | " + RobotID + " | " + Action + " | " + Description + " | " + ActionTime + " | " + Speed);

                    //                sqlite_cmd.CommandText = $"INSERT INTO {databaseTableName} ('{csvColumnNames[0]}','{csvColumnNames[1]}'," +
                    //$"'{csvColumnNames[2]}','{csvColumnNames[3]}','{csvColumnNames[4]}','{csvColumnNames[5]}') VALUES ('{Step}','{RobotID}'," +
                    //$"'{Action}','{Description}','{ActionTime}','{Speed}')";

                    //                sqlite_cmd.ExecuteNonQuery();

                    //            }
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            sqlite_conn.Close();
        }

        private string ErrorMsg(string msg)
        {
            return "[ ERROR ] :" + msg;
        }
    }
}
