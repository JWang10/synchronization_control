﻿using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Protocol;
using MQTTnet.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Geosat.Mqtt
{
    public class MqttModule
    {
        protected string currentPath = Directory.GetCurrentDirectory();
        private MqttServer mqttServer = null;
        protected MqttClient mqttClient = null;
        public string topic_receive { get; set; } = string.Empty;
        public string topic_send { get; set; } = string.Empty;
        public string brokerAddress { get; set; } = "localhost";
        public int brokerPort { get; set; } = 8883;
        public string username { get; set; } = "geosat";
        public string passsword { get; set; } = "geosat";
        //private int receivedMessages = 0;
        protected object locked = new object();
        private List<TopicFilter> subscribe_TopicList = null;

        public void ServerInit()
        {
            //var options = new MqttServerOptions();
            //var mqttServer1 = new MqttFactory().CreateMqttServer() as MqttServer;
            CreateMQTTServerAsync();
            //var options = new MqttServerOptions
            //{
            //    ConnectionValidator = c =>
            //    {
            //        if (c.ClientId.Length < 10)
            //        {
            //            return MqttConnectReturnCode.ConnectionRefusedIdentifierRejected;
            //        }

            //        if (c.Username != "xxx" || c.Password != "xxx")
            //        {
            //            return MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
            //        }

            //        return MqttConnectReturnCode.ConnectionAccepted;
            //    }
            //};

        }
        public void ServerStop()
        {
            mqttServer?.StopAsync();
        }
        public bool IsConntected()
        {
            return mqttClient.IsConnected;
        }
        /// <summary>
        /// Need to be with " broker address", "broker port", "Username", "Password"
        /// </summary>
        public void ClientInit(bool auto_reconnect = false, int CommunicationTimeout = 30, int KeepAlivePeriod = 60)
        {
            //Console.WriteLine("Input broker address: ");
            //brokerAddress = Console.ReadLine().ToLower().Trim();
            //Console.WriteLine("Input broker port: ");
            //brokerPort = Convert.ToInt32(Console.ReadLine().ToLower().Trim());
            bool check_first = true;
            var options = new MqttClientOptions
            {
                ClientId = Guid.NewGuid().ToString(),
                Credentials = new MqttClientCredentials
                {
                    Username = username,
                    Password = passsword
                },
                ChannelOptions = new MqttClientTcpOptions
                {
                    Server = brokerAddress,
                    Port = brokerPort,
                    //TlsOptions = new MqttClientTlsOptions //還沒搞清楚這怎麼使用, 只要加了這個就卡了
                    //{
                    //    UseTls = true
                    //}
                },
                ProtocolVersion = MQTTnet.Serializer.MqttProtocolVersion.V311,
                CommunicationTimeout = TimeSpan.FromSeconds(CommunicationTimeout),
                KeepAlivePeriod = TimeSpan.FromSeconds(KeepAlivePeriod),
                CleanSession = true
            };

            mqttClient = new MqttFactory().CreateMqttClient() as MqttClient;

            if (!auto_reconnect && check_first)
            {
                try
                {
                    mqttClient.ConnectAsync(options).GetAwaiter().GetResult();
                    Console.WriteLine("MQTT server connected\n");
                    Task.Run(async () => { await MqttClientReceived(); });
                    check_first = false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("MQTT server connected failed\n" + "[ERROR] " + ex.Message);
                    string msg = @"Time：" + DateTime.Now + "\n"
                                + "Message：" + "MQTT server connected failed\n" + "[ERROR] " + ex.Message + "\n"
                                + "--------------------------------------------------\n";

                    WriteMsgLog(msg);
                    return;
                }

                LoadandSubscribeTopic();
                Task.Run(async () => { await Reconnect(options, check_first); });

            }
        }
        public async Task Reconnect(MqttClientOptions options, bool check_first = false, int timeout = 1)
        {
            var connected = mqttClient.IsConnected;
            while (true)
            {
                if (!connected)
                {
                    try
                    {
                        mqttClient.ConnectAsync(options).GetAwaiter().GetResult();
                        Console.WriteLine("MQTT server connected\n");
                        //MqttClientReceived();
                        LoadandSubscribeTopic();

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("MQTT server connected failed\n" + "[ERROR] " + ex.Message);
                        string msg = @"Time：" + DateTime.Now + "\n"
                                    + "Message：" + "MQTT server connected failed\n" + "[ERROR] " + ex.Message + "\n"
                                    + "--------------------------------------------------\n";

                        WriteMsgLog(msg);
                    }
                }
  
                await Task.Delay(TimeSpan.FromMilliseconds(timeout * 1000));
                connected = mqttClient.IsConnected;
            }
        }

        public void setSubscribe(string topic,int QoS)
        {
            subscribe_TopicList = new List<TopicFilter>{
                    new TopicFilter(topic, switchQoS(QoS.ToString()))};
        }
        public void setSubscribe_List(Dictionary<string, int> topicList)
        {
            foreach (KeyValuePair<string, int> entry in topicList)
            {
                subscribe_TopicList = new List<TopicFilter>{
                    new TopicFilter(entry.Key, switchQoS(entry.Value.ToString()))};
            }
        }
        private void LoadandSubscribeTopic()
        {
            foreach (var i in subscribe_TopicList)
            {
                mqttClient.SubscribeAsync(i.Topic, i.QualityOfServiceLevel);
                Console.WriteLine($"Subscribe Topic [{i.Topic}] successfully.");
            }
        }

        /// <summary>
        /// Receive message through a topic
        /// </summary>
        public void ClientSubcribe()
        {
            if (mqttClient.IsConnected)
            {
                Console.WriteLine("Input Topic Received: ");
                topic_receive = Console.ReadLine().ToLower().Trim();
                mqttClient.SubscribeAsync(new List<TopicFilter> {
                new TopicFilter(topic_receive, MqttQualityOfServiceLevel.AtMostOnce)
                });
            }
            else
            {
                Console.WriteLine("MQTT server doesn't connected yet");

            }
        }
        /// <summary>
        /// Send message through a topic
        /// </summary>
        public void RunClient()
        {

            if (mqttClient.IsConnected)
            {
                Console.WriteLine("MQTT server connected\n");
                Console.WriteLine("Input Topic Sended: ");
                topic_send = Console.ReadLine().ToLower().Trim();
                mqttClient.SubscribeAsync(new List<TopicFilter> {
                new TopicFilter(topic_send, MqttQualityOfServiceLevel.AtMostOnce)
                });
                while (true)
                {
                    Console.WriteLine("Input Message: ");
                    var inputString = Console.ReadLine().ToLower().Trim();
                    var appMsg = new MqttApplicationMessage
                    {
                        Topic = topic_send,
                        Payload = Encoding.UTF8.GetBytes(inputString),
                        QualityOfServiceLevel = MqttQualityOfServiceLevel.AtMostOnce,
                        Retain = false
                    };
                    mqttClient.PublishAsync(appMsg);
                }
            }
            else
            {
                Console.WriteLine("MQTT server doesn't connected yet");

            }
        }

        public string Publish(string topic_send, string message, int QoS = 0)
        {
            //var Qos = MqttQualityOfServiceLevel.AtMostOnce;
            //switch (Enum.Parse(typeof(MqttQualityOfServiceLevel), QoS.ToString()))
            //{
            //    case MqttQualityOfServiceLevel.AtMostOnce:
            //        Qos = MqttQualityOfServiceLevel.AtMostOnce;
            //        break;
            //    case MqttQualityOfServiceLevel.AtLeastOnce:
            //        Qos = MqttQualityOfServiceLevel.AtLeastOnce;
            //        break;
            //    case MqttQualityOfServiceLevel.ExactlyOnce:
            //        Qos = MqttQualityOfServiceLevel.ExactlyOnce;
            //        break;
            //}

            var appMsg = new MqttApplicationMessage
            {
                Topic = topic_send,
                Payload = Encoding.UTF8.GetBytes(message),
                QualityOfServiceLevel = switchQoS(QoS.ToString()),
                Retain = false
            };
            mqttClient.PublishAsync(appMsg);
            return null;
        }

        public void Subscribe(string topic_receive, int QoS = 0)
        {
            if (mqttClient.IsConnected)
            {
                this.topic_receive = topic_receive;
                //mqttClient.SubscribeAsync(new List<TopicFilter> {
                //new TopicFilter(topic_receive, MqttQualityOfServiceLevel.AtMostOnce)
                //});
                mqttClient.SubscribeAsync(
                new TopicFilter(topic_receive, switchQoS(QoS.ToString()))
                );
                //receivedMessages++;
                Console.WriteLine($"Subscribe Topic [{topic_receive}] successfully.");
            }
            else
            {
                Console.WriteLine("MQTT server doesn't connected");
            }
        }

        private MqttQualityOfServiceLevel switchQoS(string QoS)
        {
            switch (Enum.Parse(typeof(MqttQualityOfServiceLevel), QoS))
            {
                case MqttQualityOfServiceLevel.AtMostOnce:
                    return MqttQualityOfServiceLevel.AtMostOnce;
                case MqttQualityOfServiceLevel.AtLeastOnce:
                    return MqttQualityOfServiceLevel.AtLeastOnce;
                case MqttQualityOfServiceLevel.ExactlyOnce:
                    return MqttQualityOfServiceLevel.ExactlyOnce;
            }
            return MqttQualityOfServiceLevel.AtMostOnce;
        }

        public void Unsubscribe(string topic_receive)
        {
            if (mqttClient.IsConnected)
            {
                try
                {
                    //mqttClient.UnsubscribeAsync(new List<string> { topic_receive });
                    mqttClient.UnsubscribeAsync(topic_receive);
                    //receivedMessages--;
                    Console.WriteLine($"Unsubscribe Topic [{topic_receive}] successfully.");
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ErrorMsg(ex.Message));
                    PrintToConsole(ErrorMsg(ex.Message), ConsoleColor.Red);
                }
            }
            else
            {
                Console.WriteLine("MQTT server doesn't connected");
            }
        }

        private async void CreateMQTTServerAsync()
        {
            if (mqttServer == null)
            {
                var options = new MqttServerOptions()
                {
                    ConnectionValidator = c =>
                    {
                        if (c.ClientId.Length < 10)
                        {
                            c.ReturnCode = MqttConnectReturnCode.ConnectionRefusedIdentifierRejected;
                            return;
                        }

                        if (c.Username != username)
                        {
                            c.ReturnCode = MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
                            return;
                        }

                        if (c.Password != passsword)
                        {
                            c.ReturnCode = MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
                            return;
                        }

                        c.ReturnCode = MqttConnectReturnCode.ConnectionAccepted;
                    }
                };

                options.DefaultEndpointOptions.Port = brokerPort;
                //options.TlsEndpointOptions.BoundInterNetworkAddress = System.Net.IPAddress.Parse("127.0.0.1");
                //options.TlsEndpointOptions.Port = brokerPort;
                ////options.TlsEndpointOptions.Certificate = 
                //options.TlsEndpointOptions.SslProtocol= System.Security.Authentication.SslProtocols.Tls;
                //options.TlsEndpointOptions.IsEnabled = true;

                ///無法使用TLS, 發現還需要加入certificate(同上),才可以使用TLS
                var optionsBuilder = new MqttServerOptionsBuilder();
                ///Broker Address
                //optionsBuilder.WithEncryptedEndpointBoundIPAddress(System.Net.IPAddress.Parse("127.0.0.1"));
                ///Port
                //optionsBuilder.WithEncryptedEndpointPort(brokerPort);
                optionsBuilder.WithDefaultEndpointPort(brokerPort);
                /// SSL\TLS
                //optionsBuilder.WithEncryptionSslProtocol(System.Security.Authentication.SslProtocols.Tls11);
                ///ConnectionBacklog，default 2000
                //optionsBuilder.WithConnectionBacklog(2000);
                ///
                optionsBuilder.WithConnectionValidator(c =>
                {
                    if (c.ClientId.Length < 10)
                    {
                        c.ReturnCode = MqttConnectReturnCode.ConnectionRefusedIdentifierRejected;
                        return;
                    }

                    if (c.Username != username)
                    {
                        c.ReturnCode = MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
                        return;
                    }

                    if (c.Password != passsword)
                    {
                        c.ReturnCode = MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
                        return;
                    }

                    c.ReturnCode = MqttConnectReturnCode.ConnectionAccepted;
                });

                mqttServer = new MqttFactory().CreateMqttServer() as MqttServer;

                //Add message from client to the log                     
                mqttServer.ApplicationMessageReceived += (s, e) =>
                {
                    string msg = @"Client id:" + e.ClientId + "\n"
                                + "Time：" + DateTime.Now + "\n"
                                + "Topic：" + e.ApplicationMessage.Topic + "\n"
                                + "Qos：" + e.ApplicationMessage.QualityOfServiceLevel.ToString() + "\n"
                                + "Message：" + Encoding.UTF8.GetString(e.ApplicationMessage.Payload ?? new byte[0]) + "\n"
                                + "--------------------------------------------------\n";

                    WriteMsgLog(msg);
                };
                mqttServer.ClientConnected += (s, e) =>
                {
                    Console.WriteLine($"Client id：{e.ClientId} connected");

                };
                mqttServer.ClientDisconnected += (s, e) =>
                {
                    Console.WriteLine($"Client[" +
                        $"{e.ClientId}] disconnected");
                };
                await mqttServer.StartAsync(options);
                Console.WriteLine("MQTT service activated！");
            }
        }

        ///TEST
        private async Task ConnectMqttServerAsync()
        {
            if (mqttClient == null)
            {
                mqttClient = new MqttFactory().CreateMqttClient() as MqttClient;
                mqttClient.ApplicationMessageReceived += (s, e) =>
                {

                };
                mqttClient.Connected += (s, e) =>
                {
                    Console.WriteLine("MQTT server connected");
                };
                mqttClient.Disconnected += (s, e) =>
                {
                    Console.WriteLine("MQTT server dicconnected");
                };
            }

            try
            {
                var options = new MqttClientOptions
                {
                    ClientId = Guid.NewGuid().ToString().Substring(0, 5),
                    //UserName = "u001",
                    //Password = "p001",
                    ChannelOptions = new MqttClientTcpOptions
                    {
                        Server = "localhost"
                    },
                    CleanSession = true
                };

                await mqttClient.ConnectAsync(options);
            }
            catch (Exception ex)
            {
                Console.WriteLine("MQTT server failed\n" + "[ERROR] " + ex.Message);

            }
        }

        public virtual async Task MqttClientReceived()
        {

            mqttClient.ApplicationMessageReceived += (s, e) =>
            {
                lock (locked)
                {
                    Console.WriteLine($"From Topic [{e.ApplicationMessage.Topic}]");
                    Console.WriteLine(Encoding.UTF8.GetString(e.ApplicationMessage.Payload));
                }
            };
        }

        protected string ErrorMsg(string msg)
        {
            return "[ ERROR ] :" + msg;
        }

        public void PrintToConsole(string message, ConsoleColor color = ConsoleColor.White)
        {
            lock (locked)
            {
                var backupColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.Write(message);
                Console.ForegroundColor = backupColor;
            }
        }

        #region Message Log
        /// <summary>  
        /// Message Log
        /// </summary>  
        /// <param name="msg"></param>  
        public virtual void WriteMsgLog(string msg)
        {

            //string path = @"C:\log.txt";  

            ///log 存在 windows服務 的目錄下  
            //string path = AppDomain.CurrentDomain.BaseDirectory + "\\Msglog.txt";
            string path = currentPath + "\\Msglog.txt";
            FileInfo file = new FileInfo(path);
            if (!file.Exists)
            {
                FileStream fs;
                fs = File.Create(path);
                fs.Close();
            }
            using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(DateTime.Now.ToString() + "   " + msg);
                }
            }
        }
        #endregion

    }
}
