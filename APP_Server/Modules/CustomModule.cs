﻿using Geosat.Mqtt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace APP_Server
{
    public class CustomModule : MqttModule
    {
        ///For 整點啟動 (在00秒觸發)
        private static System.Timers.Timer timerCheck;
        private static System.Timers.Timer timerLaunch;
        private string timer_mode = string.Empty;
        private double timer_time = 0.0;

        Dictionary<string, bool> clientList_jobCheck = new Dictionary<string, bool>
        {
            { "c1",false },{ "c2",false },{ "c3",false}
        };
        Dictionary<string, bool> clientList_jobCheck_bak;
        DBModule db;
        public string csvPath { get; set; } = string.Empty;
        string[] columnHead = null;

        readonly string TOPIC = ConfigurationManager.AppSettings["TOPIC"].ToString();
        readonly int PUBLICH_QoS = Int32.Parse(ConfigurationManager.AppSettings["PUBLICH_QoS"].ToString());
        //int step = 1;
        //int lastTime = 0;
        //string sum = null;
        //string sleepSet = null;
        //string[] sleepSets = null;
        //int time = 0;
        object locked = new object();

        public override async Task MqttClientReceived()
        {
            mqttClient.ApplicationMessageReceived += (s, e) =>
            {
                lock (locked)
                {
                    var msg = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
                    Console.WriteLine($"From Topic [{e.ApplicationMessage.Topic}]");
                    //Console.WriteLine(msg);
                    string[] contents = msg.Split(',');
                    string work = null;
                    //public delegate void Action;
                    Dictionary<string, Action> clientActions;
                    if (contents[0].ToLower().Trim() == ConfigurationManager.AppSettings["SERVER_ID"])
                    {
                        try
                        {
                            //work = $"[ {contents[0].ToUpper()} ] {contents[1]} {contents[2]} {contents[3]}\n";
                            work = $"[ {contents[0].ToUpper()} ] ";
                            for(int i=1; i< contents.Length;i++)
                            {
                                work += contents[i] + " ";
                            }
                            work += "\n";
                            PrintToConsole(work, ConsoleColor.Yellow);
                            //db.UpdateServerStatus();

                            #region record the status of clients
                            ///not done yet, to do record the status of clients
                           
                            //clientActions = new Dictionary<string, Action>{
                            //{ columnHead[1].ToLower().ToString(), () => {

                            //    clientList_jobCheck[columnHead[1].ToLower().ToString()] = true;

                            //    //int step1 = step;
                            //    //if (step <= Int32.Parse(sum))
                            //    //{
                            //    //    PrintToConsole("step: " + step.ToString() + "\n", ConsoleColor.Red);
                            //    //    sleepSet = db.ReadTable(step.ToString());
                            //    //    PrintToConsole(sleepSet + "\n", ConsoleColor.Cyan);
                            //    //    sleepSets = sleepSet.Split(',');
                            //    //    time = Int32.Parse(sleepSets[1]) - lastTime + 2;
                            //    //    System.Threading.Thread.Sleep(time * 1000);//for C1
                            //    //    Publish(topic, $"{columnHead[1]},{sleepSets[0].ToString()}");
                            //    //    lastTime = Int32.Parse(sleepSets[1]);
                            //    //    step++;
                            //    //}

                            //}},
                            //{ columnHead[2].ToLower().ToString(), () => {

                            //    clientList_jobCheck[columnHead[2].ToLower().ToString()] = true;

                            //    //int step2 = step;
                            //    //if (step2 <= Int32.Parse(sum))
                            //    //{
                            //    //    PrintToConsole("step: " + step2.ToString() + "\n", ConsoleColor.Red);
                            //    //    sleepSet = db.ReadTable(step2.ToString());
                            //    //    PrintToConsole(sleepSet + "\n", ConsoleColor.Cyan);
                            //    //    sleepSets = sleepSet.Split(',');
                            //    //    time = Int32.Parse(sleepSets[1]) - lastTime + 2;
                            //    //    System.Threading.Thread.Sleep(time * 1000);//for C1
                            //    //    Publish(topic, $"{columnHead[1]},{sleepSets[0].ToString()}");
                            //    //    lastTime = Int32.Parse(sleepSets[1]);
                            //    //    step2++;
                            //    //}
                            //}}};

                            //clientActions[contents[2].ToLower().ToString()].Invoke();

                            //foreach (var client in clientList_jobCheck)
                            //{
                            //    if (client.Value == true)
                            //    {

                            //        clientList_jobCheck[contents[2].ToLower().ToString()] = false;
                            //    }
                            //}
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            string log = @"Server id:" + e.ClientId + "\n"
                                        + "Time：" + DateTime.Now + "\n"
                                        + "Message：" + ErrorMsg(ex.Message) + "\n"
                                        + "--------------------------------------------------\n";
                            WriteMsgLog(log);
                            work = null;
                        }
                        finally
                        {
                            string log = @"Server id:" + e.ClientId + "\n"
                                        + "Time：" + DateTime.Now + "\n"
                                        + "Message：" + work + "\n"
                                        + "--------------------------------------------------\n";
                            WriteMsgLog(log);
                            work = null;
                        }
                    }


                }
            };
        }

        public void ShowList()
        {
            foreach (var item in clientList_jobCheck)
            {
                PrintToConsole($"{item.Key}: {item.Value}\n", ConsoleColor.Cyan);
            }
        }

        /// <summary>
        /// 將CSV文件的數據讀取到DataTable中
        /// </summary>
        /// <param name="fileName">CSV文件路徑</param>
        /// <returns>返回讀取了CSV數據的DataTable</returns>
        public DataTable LoadCSV(string filePath, out string[] columnHead)
        {
            DataTable dt = new DataTable();
            FileStream fs = new FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            StreamReader sr = new StreamReader(fs, Encoding.Default);
            //StreamReader sr = new StreamReader(fs, encoding);
            //string fileContent = sr.ReadToEnd();
            //記錄每次讀取的一行記錄
            string strLine = "";
            //記錄每行記錄中的各字段內容
            string[] aryLine = null;
            string[] tableHead = null;
            columnHead = tableHead;
            //標示列數
            int columnCount = 0;
            //標示是否是讀取的第一行
            bool IsFirst = true;
            //逐行讀取CSV中的數據
            while ((strLine = sr.ReadLine()) != null)
            {
                if (IsFirst == true)
                {
                    tableHead = strLine.Split(',');
                    columnHead = tableHead;
                    IsFirst = false;
                    columnCount = tableHead.Length;
                    //創建列
                    for (int i = 0; i < columnCount; i++)
                    {
                        tableHead[i] = tableHead[i].Replace("\"", "");
                        DataColumn dc = new DataColumn(tableHead[i]);
                        dt.Columns.Add(dc);
                    }
                }
                else
                {
                    aryLine = strLine.Split(',');
                    DataRow dr = dt.NewRow();
                    for (int j = 0; j < columnCount; j++)
                    {
                        dr[j] = aryLine[j].Replace("\"", "");
                    }
                    dt.Rows.Add(dr);
                }
            }
            //if (aryLine != null && aryLine.Length > 0)
            //{
            //    dt.DefaultView.Sort = tableHead[2] + " " + "DESC";
            //}
            sr.Close();
            fs.Close();
            return dt;
        }
        /// <summary>
        /// 將DataTable中數據寫入到CSV文件中
        /// </summary>
        /// <param name="dt">提供保存數據的DataTable</param>
        /// <param name="fileName">CSV的文件路徑</param>
        public bool SaveCSV(DataTable dt, string fullPath)
        {
            try
            {
                FileInfo fi = new FileInfo(fullPath);
                if (!fi.Directory.Exists)
                {
                    fi.Directory.Create();
                }
                FileStream fs = new FileStream(fullPath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                //StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
                StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
                string data = "";
                //寫出列名稱
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    data += "\"" + dt.Columns[i].ColumnName.ToString() + "\"";
                    if (i < dt.Columns.Count - 1)
                    {
                        data += ",";
                    }
                }
                sw.WriteLine(data);
                //寫出各行數據
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    data = "";
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        string str = dt.Rows[i][j].ToString();
                        str = string.Format("\"{0}\"", str);
                        data += str;
                        if (j < dt.Columns.Count - 1)
                        {
                            data += ",";
                        }
                    }
                    sw.WriteLine(data);
                }
                sw.Close();
                fs.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Initialnize 
        /// </summary>
        public void Init()
        {
            clientList_jobCheck_bak = clientList_jobCheck;

            var dt = LoadCSV(csvPath, out columnHead);
            //foreach (var col in columnHead)
            //    PrintToConsole(col, ConsoleColor.Cyan);
            //Console.WriteLine();
            db = new DBModule
            {
                path = @"Show.db",
                tableScript = @"ServerScript",
                dt = dt
            };
            db.InitDB();

            Console.WriteLine(db.ImportCSVtoDB(csvPath, columnHead));
            Console.WriteLine(db.ImportRobotIDtoDB(columnHead));

        }
        public async void StartScheduleAsync()
        {
            for (int index = 2; index < columnHead.Length; index++)
            {
                Task.Run(() =>
                {
                    //Console.WriteLine("進行 線程" + System.Threading.Thread.CurrentThread.ManagedThreadId);
                    SchedulePublish(ConfigurationManager.AppSettings["TOPIC"].ToString(), columnHead[index], index);
                }).GetAwaiter().GetResult();

                ///多用戶, 單線程
                //Script s = new Script() {
                //    db = db
                //};
                //s.Init();
                //s.SchedulePublish(TOPIC, columnHead[index], index);
                ///單用戶, 多線程
                //Script s = new Script() {
                //    topic = topic,
                //    whichColumnHead = columnHead[index],
                //    index = index,
                //    db = db
                //};
                //ParameterizedThreadStart para = new ParameterizedThreadStart(s.SchedulePublish);
                //Thread t1 = new Thread(para);
                ////Thread t2 = new Thread(para);
                ////Thread t3 = new Thread(para);
                //t1.Start();
                ////t2.Start();
                ////t3.Start();

            }
        }

#if false
        /// <summary>
        /// Run Script from DB to send message
        /// </summary>
        /// <param name="topic"> Publish to Channel </param>
        public async Task SchedulePublish(string topic, string whichColumnHead)
        {
               //Task.Run(async () =>
            //{
            //lock (locked)
            //{
            //Console.WriteLine("進行 線程" + System.Threading.Thread.CurrentThread.ManagedThreadId);
            Publish(topic, $"[{whichColumnHead}] Show time !");
            var step = 1;
            var time = 0;
            var lastTime = 0;
            var sum = db.ReadTableCount(whichColumnHead);
            ///Load script in first time
            var sleepSet = db.ReadTable(whichColumnHead, step.ToString());
            var sleepSets = sleepSet.Trim('/').Split(',');

            time = Int32.Parse(sleepSets[index]);

            //PrintToConsole($"[{whichColumnHead}] step: " + step.ToString() + "\n", ConsoleColor.Red);
            //PrintToConsole(time + "\n", ConsoleColor.Cyan);

            //System.Threading.Thread.Sleep(time * 1000);//for C1
            await Task.Delay(time * 1000);//for C1

            Publish(topic, $"{whichColumnHead},{sleepSets[0].ToString()}");
            PrintToConsole($"{whichColumnHead},{sleepSets[0].ToString()}" + "\n", ConsoleColor.Cyan);

            lastTime = time;
            step++;

            while (true)
            {
                if (step <= Int32.Parse(sum))
                {
                    sleepSet = db.ReadTable(whichColumnHead, step.ToString());
                    sleepSets = sleepSet.Trim('/').Split(',');

                    PrintToConsole(sleepSets[index] + "\n", ConsoleColor.Red);
                    if (Int32.Parse(sleepSets[index]) <= 0)
                        break;
                    time = Int32.Parse(sleepSets[index]) - lastTime;

                    //PrintToConsole($"[{whichColumnHead}] step: " + step.ToString() + "\n", ConsoleColor.Red);
                    //PrintToConsole(sleepSets[index] + "\n", ConsoleColor.Cyan);

                    //System.Threading.Thread.Sleep(time * 1000);//for C1
                    await Task.Delay(time * 1000);//for C1

                    Publish(topic, $"{whichColumnHead},{sleepSets[0].ToString()}");
                    PrintToConsole($"{whichColumnHead},{sleepSets[0].ToString()}" + "\n", ConsoleColor.Cyan);

                    lastTime = Int32.Parse(sleepSets[index]);
                    step++;
                }
                else
                    break;
            }
            step = 1;
            //}
            //});
        }
#endif
        public async Task SchedulePublish(string topic, string whichColumnHead, int index)
        {
            //Task.Run(async () =>
            //{
            //lock (locked)
            //{
            //Console.WriteLine("進行 線程" + System.Threading.Thread.CurrentThread.ManagedThreadId);
            Publish(topic, $"[{whichColumnHead}] Show time !");
            var step = 1;
            var time = 0;
            var lastTime = 0;
            var sum = db.ReadTableCount(whichColumnHead);
           
            ///Load script in first time
            var sleepSet = db.ReadTable(whichColumnHead, step.ToString());
            Console.WriteLine(sleepSet);

            var sleepSets = sleepSet.Trim('/').Split(',');
            var cmd_contents = sleepSets[index].Split('>');

            time = Int32.Parse(sleepSets[1]);

            PrintToConsole($"[{whichColumnHead}] step: " + step.ToString() + "\n", ConsoleColor.Red);
            //PrintToConsole(sleepSets[index] + "\n", ConsoleColor.Cyan);

            //System.Threading.Thread.Sleep(time * 1000);//for C1
            await Task.Delay(time * 1000);//for C1
            try
            {
                Publish(topic, $"{whichColumnHead}>{cmd_contents[0].ToString()}>{cmd_contents[1].ToString()}", PUBLICH_QoS);
            }
            catch
            {

            }
            //PrintToConsole($"{whichColumnHead},{sleepSets[0].ToString()}" + "\n", ConsoleColor.Cyan);

            lastTime = time;
            step++;

            while (true)
            {
                if (step <= Int32.Parse(sum))
                {
                    lock (locked)
                        sleepSet = db.ReadTable(whichColumnHead, step.ToString());
                    sleepSets = sleepSet.Trim('/').Split(',');
                    cmd_contents = sleepSets[index].Split('>');

                    if (Int32.Parse(sleepSets[1]) <= 0)
                        break;
                    time = Int32.Parse(sleepSets[1]) - lastTime;

                    PrintToConsole($"[{whichColumnHead}] step: " + step.ToString() + "\n", ConsoleColor.Red);
                    //PrintToConsole(sleepSets[index] + "\n", ConsoleColor.Cyan);

                    //System.Threading.Thread.Sleep(time * 1000);
                    await Task.Delay(time * 1000);//for C1

                    try
                    {
                        Publish(topic, $"{whichColumnHead}>{cmd_contents[0].ToString()}>{cmd_contents[1].ToString()}", PUBLICH_QoS);
                    }
                    catch
                    {

                    }
                    //PrintToConsole($"{whichColumnHead},{sleepSets[0].ToString()}" + "\n", ConsoleColor.Cyan);
                    lastTime = Int32.Parse(sleepSets[1]);
                    step++;
                }
                else
                    break;
            }
            step = 1;
            //}
            //});
        }
        public override void WriteMsgLog(string msg)
        {
            string path = currentPath + "\\Serverlog.txt";
            FileInfo file = new FileInfo(path);
            if (!file.Exists)
            {
                FileStream fs;
                fs = File.Create(path);
                fs.Close();
            }
            using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(DateTime.Now.ToString() + "   " + msg);
                }
            }
        }
        #region 整點 執行
        /// <summary>
        /// Check 在營業時間是否整點 執行
        /// </summary>
        public void StartMission(string mode,double time = 1.0)
        {
            var timeNow = DateTime.Now.Hour;
            timer_mode = mode;
            timer_time = time;
            Console.WriteLine("Check time: " + DateTime.Now);
            if (timeNow >= Int32.Parse(ConfigurationManager.AppSettings["TIME_OPEN"].ToString())
                && timeNow < Int32.Parse(ConfigurationManager.AppSettings["TIME_CLOSE"].ToString()))
            {
                if(timer_mode == "minute")
                    timerCheck = new System.Timers.Timer((60 - DateTime.Now.Second) * 1000); // 1 min
                else if(timer_mode == "hour")
                    timerCheck = new System.Timers.Timer((60 - DateTime.Now.Minute) * (60 - DateTime.Now.Second) * 1000); // 1 hour
                //int diff = ((60 - DateTime.Now.Minute) * (60 - DateTime.Now.Second)) + (60 - DateTime.Now.Second);
                //timerCheck = new System.Timers.Timer(diff * 1000); // unit: minute
                timerCheck.Elapsed += new ElapsedEventHandler(OnCheckTimedEvent);
                timerCheck.Enabled = true;
                timerCheck.AutoReset = false;
            }

        }
        private void OnCheckTimedEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);
            //Console.WriteLine("Check time: " + e.SignalTime);
            StartScheduleAsync();

            if (timer_mode == "minute")
                timerLaunch = new System.Timers.Timer(60000 * timer_time); // 1 minute
            else if(timer_mode == "hour")
                timerLaunch = new System.Timers.Timer(3600000 * timer_time); // 1 hour

            timerLaunch.Elapsed += new ElapsedEventHandler(OnLaunchTimedEvent);
            timerLaunch.Enabled = true;
        }
        private void OnLaunchTimedEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Launch time: " + DateTime.Now);
            StartScheduleAsync();
        }
        #endregion
    }

}

