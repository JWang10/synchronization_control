﻿using Geosat.Mqtt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomModule mqtt = null;
            //if (args.Length < 1)
            //{
            //    Console.WriteLine("Usage: {0} <path-to-script> *timer *timer_times", AppDomain.CurrentDomain.FriendlyName);
            //    Console.WriteLine("Usage: * is optional, provide two modes(default unit: 1): 'minute, hour', timer_times(default: 1):how long you want");
            //    Console.WriteLine("Error: you need to add on the script launched");
            //    Environment.Exit(0); //exitcode: 0 is success, anything else indicates an error, ref: http://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx
            //}

            mqtt = new CustomModule
            {
                brokerAddress = ConfigurationManager.AppSettings["SERVER"],
                brokerPort = Convert.ToInt32(ConfigurationManager.AppSettings["PORT"]),
                username = ConfigurationManager.AppSettings["USER"],
                passsword = ConfigurationManager.AppSettings["PASS"],
                //csvPath = args[0]
                csvPath = "server.csv"

            };

            try
            {
                if (ConfigurationManager.AppSettings["AUTOSEARCH"] == "true")
                {
                    /// launch DNS discovery service
                    var dns_service = new DiscoverDNS.DiscoverDNS(); //AUTOSEARCH
                    dns_service.BasedQuery("mqtt");
                    while (true)
                        if (dns_service.ipAddress != string.Empty)
                            break;
                    mqtt.brokerAddress = dns_service.ipAddress;
                }
                if (ConfigurationManager.AppSettings["SERVER"] != "")
                    mqtt.brokerAddress = ConfigurationManager.AppSettings["SERVER"];

                mqtt.Init();
                mqtt.setSubscribe("server", Int32.Parse(ConfigurationManager.AppSettings["SUBSCRIBE_QoS"].ToString()));
                mqtt.ClientInit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Error] " + ex.Message);
                Environment.Exit(0);
            }

            //if (args.Length < 2)
            //    mqtt.StartScheduleAsync();
            //else if (args.Length < 3)
            //    mqtt.StartMission(args[1]);
            //else
            //    mqtt.StartMission(args[1], float.Parse(args[2]));

            var loop = true;
            while (mqtt.IsConntected() && loop)
            {
                //Console.WriteLine("Input Message: ");
                var input = Console.ReadLine().Trim();
                string[] inputs = input.Split(' ');
                switch (inputs[0].ToLower())
                {
                    case "help":
                        Console.WriteLine("Usage: <input>\n" +
                                        "\texit: exit the program\n" +
                                        "\twork *mode *times: launch the script you loaded, \n\t( Usage: * is optional, provide two modes: 'minute, hour', timer_times(default: 1):how long you want)\n" +
                                        "\tload <path-to-script>: load a script\n" +
                                        "\tpublish {msg}: send the message through mqtt");
                        break;
                    case "exit":
                        Console.WriteLine("bye bye！");
                        loop = false;
                        break;
                    case "work":
                        if (inputs.Length < 2)
                        {
                            mqtt.StartScheduleAsync();
                            //Console.WriteLine("main");
                        }
                        else if (inputs.Length < 3)  // 尚未加入停止功能，目前只能關掉重開
                        {
                            mqtt.StartMission(inputs[1]);
                            //Console.WriteLine(inputs[1]);
                        }
                        else
                        {
                            mqtt.StartMission(inputs[1], float.Parse(inputs[2]));
                            //Console.WriteLine($"{inputs[1]}, {inputs[2]}");
                        }
                        break;
                    case "load":
                        if (inputs.Length < 2)
                        {
                            mqtt.PrintToConsole("you could enter a script\n" +
                                "e.g. load <path-to-script>\n",ConsoleColor.Yellow);
                            break;
                        }
                        try
                        {
                            mqtt.csvPath = inputs[1];
                            mqtt.Init();
                        }
                        catch(Exception ex)
                        {
                            mqtt.PrintToConsole($"[ERROR] {ex.Message}\n", ConsoleColor.Red);
                            string log = @"Time：" + DateTime.Now + "\n"
                                        + "Message：" + $"[ERROR] {ex.Message}" + "\n"
                                        + "--------------------------------------------------\n";
                            mqtt.WriteMsgLog(log);
                            break;
                        }
                        break;
                    case "publish":
                        mqtt.Publish(ConfigurationManager.AppSettings["TOPIC"], inputs[1]);
                        break;
                }
            }
            System.Threading.Thread.Sleep(1000);
#if false
            string inputString, topic_send, topic_receive = string.Empty;

            Console.WriteLine("enter \"help\" if you don't know how to do.");
            while (true)
            {
                inputString = Console.ReadLine().ToLower().Trim();

                if (inputString == "help")
                {
                    Console.WriteLine("exit : exit the program");
                    Console.WriteLine("clear : clear messages from the window");
                    Console.WriteLine("publish : send the message with the topic you set");
                    Console.WriteLine("subscribe : receive the message with the topic you set");
                    Console.WriteLine("unsubcribe : unsubcribe the message with the topic you set");
                    Console.WriteLine("show : display the status of client");
                }
                switch (inputString)
                {
                    case "exit":
                        Console.WriteLine("MQTT Client exit！");
                        Environment.Exit(1);
                        break;
                    case "clear":
                        Console.Clear();
                        Console.WriteLine("enter \"help\" if you don't know how to do.");
                        break;
                    case "publish":
                        Console.WriteLine("Input Topic Sended: ");
                        topic_send = Console.ReadLine().ToLower().Trim();
                        while (true)
                        {
                            Console.WriteLine("Input Message: ");
                            var msg = Console.ReadLine().ToLower().Trim();
                            if (msg == "exit")
                                break;
                            mqtt.Publish(topic_send, msg);
                        }
                        break;
                    case "subscribe":
                        Console.WriteLine("Input Topic Received: ");
                        topic_receive = Console.ReadLine().ToLower().Trim();
                        mqtt.Subscribe(topic_receive);
                        //while (true)
                        //{
                        //    inputString = Console.ReadLine().ToLower().Trim();
                        //    if (inputString == "exit")
                        //        break;
                        //}
                        break;
                    case "unsubcribe":
                        Console.WriteLine("Input Topic Received: ");
                        topic_receive = Console.ReadLine().ToLower().Trim();
                        mqtt.Unsubscribe(topic_receive);
                        break;
                    case "show":
                        mqtt.ShowList();
                        break;
                }
                //Console.WriteLine("Press any key to exit.");
                //Console.ReadLine();
            }
#endif
        }
    }
}
