﻿using Geosat.Mqtt;
using MQTTnet.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTT_Client
{
    class Program
    {

        static void Main(string[] args)
        {
            MqttModule mqtt = new MqttModule();
            Console.WriteLine("Input broker address: ");
            mqtt.brokerAddress = Console.ReadLine().ToLower().Trim();
            Console.WriteLine("Input broker port: ");
            mqtt.brokerPort = Convert.ToInt32(Console.ReadLine().ToLower().Trim());
            Console.WriteLine("Input username: ");
            mqtt.username = Console.ReadLine().ToLower().Trim();
            Console.WriteLine("Input password: ");
            mqtt.passsword = Console.ReadLine().ToLower().Trim();

            mqtt.ClientInit();
            mqtt.RunClient();
            Console.ReadKey();
        }

    }
}
