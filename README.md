## Robot sync control demo

[TOC]

### 目的

提供多台機器人同時執行指令。以腳本或是單條指令操作機器人

#### 運作模式

- 以mqtt相互溝通
- 採用老師學生模式，老師說一句，學生做一個動作。換言之，學生只負責執行。

### 操作方式

> First，在同網域下，要有一台啟用mqtt broker server。之後的client、server會自動抓取broker

#### Client

> `config`預先設定`TOPIC`, `CLIENT_ID` , `API_URL`。`TOPIC`、`CLIENT_ID`跟`server`連接使用，`API_URL`則是搭配geosat webAPI

- 目前擁有的指令模式
    - `ROBOTBODYMOVE`：`<Direction>/<Rotation>`
        - 參數說明
            - `Direction`: 單位為`meter`，正/負號為前進/後退（e.g. 1.5/0）
            - `Rotation`：單位為`degree`，正/負號為順時針/逆時針旋轉（e.g. 0/90）
    - `ROBOTNECKMOVE`：`<Direction>/<Rotation>`
        - 參數說明
            - `Direction`: -20到20，負號為抬頭，0表示停止
            - `Rotation`：-20到20，負號為逆時針旋轉，0表示停止
    - `EMOTION`：`<type-of-emotion>`
        - 參數對應`API`的九種表情
    - `PLAYVIDEO`：`<video>/<mode>`
        - 需要搭配導覽程式
        - 參數說明
            - `video`：對應導覽程式的影片欄位
            - `mode`：播放一次`once`、循環`loop`
    - `API`
        - 直接執行`API`指令
        - 格式為`API deviceID/start(or stop)/content`
    - `CMD`
        - 直接透過CLI執行特定腳本或檔案
- 打開即可

#### Server

![](assets/mqtt-show-demo-server.png)

- 輸入`help`：呼叫指令介面
- `load`：讀取csv腳本（預設讀取`server.csv`腳本）
- `work`：執行劇本。後面參數為**定時執行**劇本：
    - mode：
        - `minute`為每分鐘執行一次
        - `hour`為每小時執行一次
    - times：
        - 預設為`1`。
    - 定時執行時間為`times * 1 <mode>`
        - e.g. mode: minute, times:5 為每5分鐘一次
- `publish`：單次發送指令
    - `client`格式為`publish {robotID}>{clientCommad}>{msg}`
    - 一般格式為`publish <msg>`
- `exit`：離開程式

