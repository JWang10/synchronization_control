﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThread
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadStart childref = new ThreadStart(CallToChildThread);
            Console.WriteLine("In Main: Creating the Child thread");
            Console.WriteLine("進行 線程" + System.Threading.Thread.CurrentThread.ManagedThreadId);
            Thread childThread = new Thread(childref);
            childThread.Start();
            Console.ReadKey();
        }
        public static void CallToChildThread()
        {
            Console.WriteLine("進行 線程" + System.Threading.Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Child thread starts");

        }
    }
}
