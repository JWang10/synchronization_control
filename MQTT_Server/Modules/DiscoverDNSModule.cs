﻿using Common.Logging;
using Common.Logging.Simple;
using Makaretu.Dns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DiscoverDNS
{
    public class DiscoverDNS
    {
        public void Service()
        {
            Console.WriteLine("Multicast DNS spike");

            // set logger factory
            //var properties = new Common.Logging.Configuration.NameValueCollection
            //{
            //    ["level"] = "TRACE",
            //    ["showLogName"] = "true",
            //    ["showDateTime"] = "true",
            //    ["dateTimeFormat"] = "HH:mm:ss.fff"

            //};
            //LogManager.Adapter = new ConsoleOutLoggerFactoryAdapter(properties);

            var mdns = new MulticastService();

            foreach (var a in MulticastService.GetIPAddresses())
            {
                Console.WriteLine($"IP address {a}");
            }

            mdns.QueryReceived += (s, e) =>
            {
                var service = "mqtt";
                var msg = e.Message;
                var names = msg.Questions.Select(q => q.Name + " " + q.Type);
                //Console.WriteLine($"got a query for {String.Join(", ", names)}");
                if (msg.Questions.Any(q => q.Name == service))
                {
                    var res = msg.CreateResponse();
                    var addresses = MulticastService.GetIPAddresses()
                        .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork);
                    foreach (var address in addresses)
                    {
                        res.Answers.Add(new ARecord
                        {
                            Name = service,
                            Address = address
                        });
                    }
                    mdns.SendAnswer(res);
                }
            };

            //mdns.AnswerReceived += (s, e) =>
            //{
            //    var names = e.Message.Answers
            //        .Select(q => q.Name + " " + q.Type)
            //        .Distinct();
            //    Console.WriteLine($"got answer for {String.Join(", ", names)}");
            //};
            mdns.NetworkInterfaceDiscovered += (s, e) =>
            {
                foreach (var nic in e.NetworkInterfaces)
                {
                    Console.WriteLine($"discovered NIC '{nic.Name}'");
                }
            };

            var sd = new ServiceDiscovery(mdns);
            sd.Advertise(new ServiceProfile("x1", "_mqtt._tcp", 5011));
            //sd.Advertise(new ServiceProfile("x2", "_xservice._tcp", 666));
            //var z1 = new ServiceProfile("z1", "_zservice.udp", 5012);
            //z1.AddProperty("foo", "bar");
            //sd.Advertise(z1);
            mdns.Start();

        }

        public void BasedQuery(string serviceNmae = "mqtt")
        {
            var mdns = new MulticastService();
            mdns.NetworkInterfaceDiscovered += (s, e) => mdns.SendQuery(serviceNmae);
            mdns.AnswerReceived += (s, e) =>
            {
                Console.WriteLine(((ARecord)e.Message.Answers[0]).Address);
            };
            mdns.Start();
        }
    }
}
