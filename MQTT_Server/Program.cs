﻿using MQTTnet;
using MQTTnet.Protocol;
using MQTTnet.Server;
using Geosat.Mqtt;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Configuration;
using System.Net;
using System.Linq;

namespace MQTT_Application
{
    class Program
    {

        static void Main(string[] args)
        {

            MqttModule mqtt = new MqttModule {
                brokerPort = Convert.ToInt32(ConfigurationManager.AppSettings["PORT"]),
                username = ConfigurationManager.AppSettings["USER"],
                passsword = ConfigurationManager.AppSettings["PASS"]

            };
            //Console.WriteLine("Input broker port: ");
            //mqtt.brokerPort = Convert.ToInt32(Console.ReadLine().ToLower().Trim());
            //Console.WriteLine("Input username: ");
            //mqtt.username = Console.ReadLine().ToLower().Trim();
            //Console.WriteLine("Input password: ");
            //mqtt.passsword = Console.ReadLine().ToLower().Trim();
            try
            {
                /// launch DNS service
                var dns_service = new DiscoverDNS.DiscoverDNS();
                dns_service.Service();

                GetIP();
                mqtt.ServerInit();
            }catch(Exception ex)
            {
                Console.WriteLine("Error, " + ex.Message);
                mqtt.ServerStop();
                Environment.Exit(0);
            }
            //new Thread(StartMqttServer).Start();

            while (true)
            {
                var inputString = Console.ReadLine().ToLower().Trim();

                if (inputString == "exit")
                {
                    mqtt.ServerStop();
                    Console.WriteLine("MQTT service has stop！");
                    break;
                }
                else if (inputString == "clients")
                {
                    //foreach (var item in mqttServer.GetClientSessionsStatus())
                    //{
                    //    Console.WriteLine($"Client id：{item.ClientId}，Protocal Version：{item.ProtocolVersion}");
                    //}

                    //if (mqttServer != null)
                    //{
                    //    List<ConnectedMqttClient> subclients = mqttServer.GetConnectedClientsAsync().Result.ToList();
                    //    //if (subclients.Count > 0)
                    //    //{
                    //    //    foreach (var item in subclients)
                    //    //    {
                    //    //        if (!subClientIDs.Contains(item.ClientId))
                    //    //        {
                    //    //            subClientIDs.Add(item.ClientId);
                    //    //            string msg = @"接收客户端ID:" + item.ClientId + "\n"
                    //    //            + "接收时间：" + DateTime.Now + "\n"
                    //    //            + "协议版本：" + item.ProtocolVersion + "\n"
                    //    //            + "最后收到的非保持活包" + item.LastNonKeepAlivePacketReceived + "\n"
                    //    //            + "最后收到的包" + item.LastPacketReceived + "\n"
                    //    //            + "挂起的应用程序消息" + item.PendingApplicationMessages + "\n"
                    //    //            + "------------------------------------------------" + "\n";
                    //    //            WriteClientLinkLog(msg);
                    //    //        }
                    //    //    }
                    //    //    if (subClientIDs.Count >= 1000)
                    //    //    {
                    //    //        subClientIDs.Clear();
                    //    //    }
                    //    //}
                    //}
                }
                else
                {
                    Console.WriteLine($"Command[{inputString}] was undefined！");
                }
            }

            //mqtt.ServerStop();
        }

        static void GetIP()
        {
            // 取得本機名稱
            string strHostName = Dns.GetHostName();
            // 取得本機的IpHostEntry類別實體，MSDN建議新的用法
            IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);
            // 本機的IP集合
            var localIp = iphostentry.AddressList.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToList();

            for (int i = 0; i < localIp.Count; i++)
            {
                Console.WriteLine($"IP {i + 1}:  {localIp[i]}");
            }
        }
    }

}
