﻿using Geosat.Mqtt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTT_Client_receive
{
    class Program
    {
        static void Main(string[] args)
        {
            MqttModule mqtt = new MqttModule();
            Console.WriteLine("Input broker address: ");
            mqtt.brokerAddress = Console.ReadLine().ToLower().Trim();
            Console.WriteLine("Input broker port: ");
            mqtt.brokerPort = Convert.ToInt32(Console.ReadLine().ToLower().Trim());
            Console.WriteLine("Input username: ");
            mqtt.username = Console.ReadLine().ToLower().Trim();
            Console.WriteLine("Input password: ");
            mqtt.passsword = Console.ReadLine().ToLower().Trim();

            mqtt.ClientInit();
            mqtt.ClientSubcribe();
            while (true)
            {
                var inputString = Console.ReadLine().ToLower().Trim();

                if (inputString == "exit")
                {
                    Console.WriteLine("MQTT Client exit！");
                    break;
                }
                //Console.WriteLine("Press any key to exit.");
                //Console.ReadLine();
            }
        }
    }
}
